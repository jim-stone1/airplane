class Airplane:
    def __init__ (self, name, seats_number):
        self.name = name
        self.seats_number = seats_number
        self.mileage_total = 0
        self.seats_taken = 0

    def fly(self, distance):
        self.mileage_total += distance

    def is_service_required(self):
        if self.mileage_total > 10000:
            return True
        else:
            return False

    def board_passengers(self, number_of_passengers):
        if number_of_passengers <= self.get_available_seats():
            self.seats_taken += number_of_passengers
        else:
            # number_of_passengers > self.get_available_seats()
            self.seats_taken += self.get_available_seats()

    def get_available_seats(self):
        return self.seats_number - self.seats_taken


class Airline:
    def __init__(self, airplanes):
        self.airplanes = airplanes

    def calc_available_seats(self):
        return sum([p.get_available_seats() for p in self.airplanes])


if __name__ == '__main__':
    p1 = Airplane('Airbus 123', 300)
    p2 = Airplane('Boeing 123', 500)
    lot = Airline([p1, p2])
    print(
        p1.board_passengers(100),
        p1.fly(5000),
        p1.seats_taken,
        p1.mileage_total,
        p1.is_service_required(),
        p1.get_available_seats(),
        lot.calc_available_seats()
    )