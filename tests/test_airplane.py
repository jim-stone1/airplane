import pytest
from src.airplane import Airplane


'''W package tests napisz testy dla klasy Airplane. Proszę o sprawdzenie następujących przypadków:
Nowo utworzony samolot ma “przebieg” 0 i ilość zajętych siedzeń 0
Samolot po przeleceniu 5000 km ma wylatane 5000 km
Samolot po przeleceniu 5000 km a następnie 3000 km ma wylatane 8000 km
Samolot po 9999 km nie wymaga serwisu
Samolot po 10001 km wymaga serwisu
Samolot o maksymalnej ilości siedzeń 200 po załadowaniu 180 pasażerów ma dostępne 20 miejsc
Samolot o maksymalnej ilości siedzeń 200 po załadowaniu 201 pasażerów ma dostępne 0 miejsc i zajętych 200 miejsc'''



def test_new_plane():
    plane = Airplane ('ABC 123', 500)
    assert plane.mileage_total == 0
    assert plane.seats_taken == 0


def test_flying_increases_mileage():
    plane = Airplane ('ABC 123', 500)
    plane.fly(5000)
    assert plane.mileage_total == 5000
    plane.fly(3000)
    assert plane.mileage_total == 8000

def test_service_9999():
    plane = Airplane ('ABC 123', 500)
    plane.fly(9999)
    assert plane.is_service_required() == False

def test_service_10001():
    plane = Airplane ('ABC 123', 500)
    plane.fly(10001)
    assert plane.is_service_required() == True

def test_boarding1():
    plane = Airplane('ABC 123', 200)
    plane.board_passengers(180)
    assert plane.get_available_seats() == 20

def test_boarding2():
    plane = Airplane('ABC 123', 200)
    plane.board_passengers(201)
    assert plane.get_available_seats() == 0
    assert plane.seats_taken == 200